## Connecting and Importing Database
- Create database my_employee
- Change database configuration in .env file (DB_DATABASE, DB_USERNAME, DB_PASSWORD)
- Using Laravel built-ins:
  - Run `php artisan migrate --seed` in terminal to migrate database
- Manual way:
  - Import my_employee.sql included in this project

## Run Project
- Run `php -S localhost:8000 -t public` in terminal

## See Postman Documentation
- Import MyEmployee.postman_collection.json in Postman
