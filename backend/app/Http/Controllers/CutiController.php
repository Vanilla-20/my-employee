<?php

namespace App\Http\Controllers;

use App\Models\Cuti;
use DateTime;
use Illuminate\Http\Request;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_cuti = Cuti::with('Pegawai')->get();
        return response()->json([
            'code'  => 200,
            'data'  => $data_cuti,
        ])->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // cek sisa cuti
        $date = DateTime::createFromFormat("Y-m-d", $request->tanggal_cuti);
        $year = $date->format("Y");
        $remaining_leaves = $this->check_remaining_leaves($request->nomor_induk, $year);
        if ($remaining_leaves < $request->lama_cuti) {
            return response()->json([
                'code'  => 400,
                'message' => "Jatah cuti habis. Sisa jatah cuti: $remaining_leaves",
            ])->setStatusCode(400);
        }
        $result = Cuti::insert($request->all());
        // cek berhasil insert
        if ($result === false) {
            return response()->json([
                'code'  => 500,
                'message' => 'Terjadi kesalahan, silakan coba lagi',
            ])->setStatusCode(500);
        }
        // response jika berhasil insert
        return response()->json([
            'code'  => 201,
            'message' => 'Cuti berhasil ditambahkan',
        ])->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cuti = Cuti::with('Pegawai')->find($id);
        // cek cuti ditemukan
        if ($cuti === null) {
            return response()->json([
                'code'  => 404,
                'message' => 'Cuti tidak ditemukan',
            ])->setStatusCode(404);
        }
        // response jika cuti berhasil ditemukan
        return response()->json([
            'code'  => 200,
            'data'  => $cuti,
        ])->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cuti = Cuti::find($id);
        // cek cuti ditemukan
        if ($cuti === null) {
            return response()->json([
                'code'  => 404,
                'message' => 'Cuti tidak ditemukan',
            ])->setStatusCode(404);
        }
        // cek sisa cuti
        $date = DateTime::createFromFormat("Y-m-d", $request->tanggal_cuti);
        $year = $date->format("Y");
        $remaining_leaves = $this->check_remaining_leaves($request->nomor_induk, $year);
        if ($remaining_leaves + $cuti->lama_cuti < $request->lama_cuti) {
            return response()->json([
                'code'  => 400,
                'message' => "Jatah cuti habis. Sisa jatah cuti: $remaining_leaves",
            ])->setStatusCode(400);
        }
        // update
        $result = $cuti->update($request->except(['id']));
        // cek berhasil update
        if ($result === false) {
            return response()->json([
                'code'  => 500,
                'message' => 'Terjadi kesalahan, silakan coba lagi',
            ])->setStatusCode(500);
        }
        // response jika berhasil update
        return response()->json([
            'code'  => 200,
            'message' => 'Cuti berhasil diubah',
        ])->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cuti = Cuti::find($id);
        // cek cuti ditemukan
        if ($cuti === null) {
            return response()->json([
                'code'  => 404,
                'message' => 'Cuti tidak ditemukan',
            ])->setStatusCode(404);
        }
        // delete
        $result = $cuti->delete();
        // cek berhasil delete
        if ($result === false) {
            return response()->json([
                'code'  => 500,
                'message' => 'Terjadi kesalahan, silakan coba lagi',
            ])->setStatusCode(500);
        }
        // response jika berhasil delete
        return response()->json([
            'code'  => 200,
            'message' => 'Cuti berhasil dihapus',
        ])->setStatusCode(200);
    }

    // ===================================================================
    private function check_remaining_leaves($nomor_induk, $year)
    {
        $leaves_count = array_sum(Cuti::whereRaw('YEAR(tanggal_cuti)=?', $year)->where('nomor_induk', '=', $nomor_induk)->pluck('lama_cuti')->toArray());
        return 12 - $leaves_count;
    }
}
