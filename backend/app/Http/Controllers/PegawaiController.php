<?php

namespace App\Http\Controllers;

use App\Models\Cuti;
use App\Models\Pegawai;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_pegawai = Pegawai::all();
        return response()->json([
            'code'  => 200,
            'data'  => $data_pegawai,
        ])->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // autogenerate nomor induk
        $nomor_induk_max = (int)substr(Pegawai::withTrashed()->orderby('nomor_induk', 'desc')->limit(1)->pluck('nomor_induk')[0], 2) + 1;
        $nomor_induk = 'IP'.str_pad($nomor_induk_max, 5, '0', STR_PAD_LEFT);
        // insert
        $pegawai = $request->all();
        $pegawai['nomor_induk'] = $nomor_induk;
        $result = Pegawai::insert($pegawai);
        // cek berhasil insert
        if ($result === false) {
            return response()->json([
                'code'  => 500,
                'message' => 'Terjadi kesalahan, silakan coba lagi',
            ])->setStatusCode(500);
        }
        // response jika berhasil insert
        return response()->json([
            'code'  => 201,
            'message' => 'Pegawai berhasil ditambahkan',
        ])->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pegawai = Pegawai::with('Cuti')->find($id);
        // cek pegawai ditemukan
        if ($pegawai === null) {
            return response()->json([
                'code'  => 404,
                'message' => 'Pegawai tidak ditemukan',
            ])->setStatusCode(404);
        }
        // response jika pegawai berhasil ditemukan
        return response()->json([
            'code'  => 200,
            'data'  => $pegawai,
        ])->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pegawai = Pegawai::find($id);
        // cek pegawai ditemukan
        if ($pegawai === null) {
            return response()->json([
                'code'  => 404,
                'message' => 'Pegawai tidak ditemukan',
            ])->setStatusCode(404);
        }
        // update
        $result = $pegawai->update($request->except(['nomor_induk']));
        // cek berhasil update
        if ($result === false) {
            return response()->json([
                'code'  => 500,
                'message' => 'Terjadi kesalahan, silakan coba lagi',
            ])->setStatusCode(500);
        }
        // response jika berhasil update
        return response()->json([
            'code'  => 200,
            'message' => 'Pegawai berhasil diubah',
        ])->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::find($id);
        // cek pegawai ditemukan
        if ($pegawai === null) {
            return response()->json([
                'code'  => 404,
                'message' => 'Pegawai tidak ditemukan',
            ])->setStatusCode(404);
        }
        // delete
        $result = $pegawai->delete();
        // cek berhasil delete
        if ($result === false) {
            return response()->json([
                'code'  => 500,
                'message' => 'Terjadi kesalahan, silakan coba lagi',
            ])->setStatusCode(500);
        }
        // response jika berhasil delete
        return response()->json([
            'code'  => 200,
            'message' => 'Pegawai berhasil dihapus',
        ])->setStatusCode(200);
    }
    
    // ====================== STATS ========================
    public function first_joining(Request $request)
    {
        $qty = $request->query('qty', 3);
        $data_pegawai = Pegawai::select(['nomor_induk', 'nama', 'tanggal_bergabung'])
                               ->orderBy('tanggal_bergabung', 'asc')
                               ->limit($qty)
                               ->get();
        return response()->json([
            'code'  => 200,
            'data' => $data_pegawai,
        ])->setStatusCode(200);
    }

    public function have_ever_left()
    {
        $data_pegawai = Cuti::with('Pegawai')->get();
        return response()->json([
            'code'  => 200,
            'data' => $data_pegawai,
        ])->setStatusCode(200);
    }

    public function have_ever_left2()
    {
        $pegawai_pernah_cuti = Cuti::groupBy('nomor_induk')
                                   ->havingRaw('COUNT(nomor_induk) > ?', [1])
                                   ->distinct()
                                   ->pluck('nomor_induk');
        $data_pegawai = Cuti::with('Pegawai')
                            ->whereIn('nomor_induk', $pegawai_pernah_cuti)
                            ->get();
        return response()->json([
            'code'  => 200,
            'data' => $data_pegawai,
        ])->setStatusCode(200);
    }

    public function remaining_leaves(Request $request)
    {
        $year = $request->query('year', Carbon::now()->year);
        $total_leaves = Cuti::select('nomor_induk')->selectRaw('SUM(lama_cuti) AS total_cuti')
                          ->whereRaw('YEAR(tanggal_cuti)=?', $year)
                          ->groupBy('nomor_induk');
        $remaining_leaves = Pegawai::select(['pegawai.nomor_induk', 'pegawai.nama'])->selectRaw('12-IFNULL(c.total_cuti,0) AS sisa_cuti')
                                   ->leftJoinSub($total_leaves, 'c', function($join) {
                                        $join->on('pegawai.nomor_induk', '=', 'c.nomor_induk');
                                    })
                                   ->get();
        return response()->json([
            'code'  => 200,
            'data' => $remaining_leaves,
        ])->setStatusCode(200);
    }
}
