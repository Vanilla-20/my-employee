<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuti extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'cuti';
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function Pegawai()
    {
        return $this->belongsTo(Pegawai::class, 'nomor_induk', 'nomor_induk');
    }
}
