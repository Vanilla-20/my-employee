<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pegawai extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'pegawai';
    protected $primaryKey = 'nomor_induk';
    protected $keyType = 'string';
    public $timestamps = true;
    protected $guarded = ['nomor_induk'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function Cuti()
    {
        return $this->hasMany(Cuti::class, 'nomor_induk', 'nomor_induk');
    }
}
