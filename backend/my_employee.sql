/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.22-MariaDB : Database - my_employee
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`my_employee` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `my_employee`;

/*Table structure for table `cuti` */

DROP TABLE IF EXISTS `cuti`;

CREATE TABLE `cuti` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_induk` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_cuti` date NOT NULL,
  `lama_cuti` int(11) NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cuti` */

insert  into `cuti`(`id`,`nomor_induk`,`tanggal_cuti`,`lama_cuti`,`keterangan`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'IP06001','2020-08-02',2,'Acara Keluarga',NULL,NULL,NULL),
(2,'IP06001','2020-08-18',2,'Anak Sakit',NULL,NULL,NULL),
(3,'IP06006','2020-08-19',1,'Nenek Sakit',NULL,NULL,NULL),
(4,'IP06007','2020-08-23',1,'Sakit',NULL,NULL,NULL),
(5,'IP06004','2020-08-29',5,'Menikah',NULL,NULL,NULL),
(6,'IP06003','2020-08-30',2,'Acara Keluarga',NULL,NULL,NULL);

/*Table structure for table `pegawai` */

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `nomor_induk` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tanggal_bergabung` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`nomor_induk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pegawai` */

insert  into `pegawai`(`nomor_induk`,`nama`,`alamat`,`tanggal_lahir`,`tanggal_bergabung`,`created_at`,`updated_at`,`deleted_at`) values 
('IP06001','Agus','Jln Gaja Mada no 12, Surabaya','1980-01-11','2005-08-07',NULL,NULL,NULL),
('IP06002','Amin','Jln Imam Bonjol no 11, Mojokerto','1977-09-03','2005-08-07',NULL,NULL,NULL),
('IP06003','Yusuf','Jln A Yani Raya 15 No 14 Malang','1973-08-09','2006-08-07',NULL,NULL,NULL),
('IP06004','Alyssa','Jln Bungur Sari V no 166, Bandung','1983-03-18','2006-09-06',NULL,NULL,NULL),
('IP06005','Maulana','Jln Candi Agung, No 78 Gg 5, Jakarta','1978-11-10','2006-09-10',NULL,NULL,NULL),
('IP06006','Agfika','Jln Nangka, Jakarta Timur','1979-02-07','2007-01-02',NULL,NULL,NULL),
('IP06007','James','Jln Merpati, 8 Surabaya','1989-05-18','2007-04-04',NULL,NULL,NULL),
('IP06008','Octavanus','Jln A Yani 17, B 08 Sidoarjo','1985-04-14','2007-05-19',NULL,NULL,NULL),
('IP06009','Nugroho','Jln Duren tiga 167, Jakarta Selatan','1984-01-01','2008-01-16',NULL,NULL,NULL),
('IP06010','Raisa','Jln Kelapa Sawit, Jakarta Selatan','1990-12-17','2008-08-16',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
