<?php

use App\Http\Controllers\CutiController;
use App\Http\Controllers\PegawaiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'pegawai'], function() {
    Route::get('/', [PegawaiController::class, 'index']);
    Route::post('/', [PegawaiController::class, 'store']);

    Route::group(['prefix' => '{id}'], function() {
        Route::get('/', [PegawaiController::class, 'show']);
        Route::put('/', [PegawaiController::class, 'update']);
        Route::delete('/', [PegawaiController::class, 'destroy']);
    });

    Route::group(['prefix' => 'stats'], function() {
        Route::get('/first_joining', [PegawaiController::class, 'first_joining']);
        Route::get('/have_ever_left', [PegawaiController::class, 'have_ever_left']);
        Route::get('/have_ever_left2', [PegawaiController::class, 'have_ever_left2']);
        Route::get('/remaining_leaves', [PegawaiController::class, 'remaining_leaves']);
    });
});

Route::group(['prefix' => 'cuti'], function() {
    Route::get('/', [CutiController::class, 'index']);
    Route::post('/', [CutiController::class, 'store']);

    Route::group(['prefix' => '{id}'], function() {
        Route::get('/', [CutiController::class, 'show']);
        Route::put('/', [CutiController::class, 'update']);
        Route::delete('/', [CutiController::class, 'destroy']);
    });
});