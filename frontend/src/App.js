import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

// import components
import Dashboard from './pages/Dashboard';
import Pegawai from './pages/pegawai/Pegawai';
import Cuti from './pages/cuti/Cuti';
import AddPegawai from './pages/pegawai/AddPegawai';
import EditPegawai from './pages/pegawai/EditPegawai ';
import AddCuti from './pages/cuti/AddCuti';
import EditCuti from './pages/cuti/EditCuti';

const App = () => {

  return (
    <Router>
      <Routes>
        <Route path='/' key='dashboard-route' element={<Dashboard title='Dashboard' />} />
        <Route path='/pegawai' key={Date.now()} element={<Pegawai title='Pegawai' />} />
        <Route path='/pegawai/add' key='add-pegawai-route' element={<AddPegawai title='Tambah Pegawai' />} />
        <Route path='/pegawai/:id/edit' key='edit-pegawai-route' element={<EditPegawai title='Ubah Pegawai' />} />
        <Route path='/cuti' key={Date.now()} element={<Cuti title='Cuti' />} />
        <Route path='/cuti/add' key='add-cuti-route' element={<AddCuti title='Tambah Cuti' />} />
        <Route path='/cuti/:id/edit' key='edit-cuti-route' element={<EditCuti title='Ubah Cuti' />} />
      </Routes>
    </Router>
  );
};

export default App;
