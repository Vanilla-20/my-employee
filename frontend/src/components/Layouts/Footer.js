const Footer = () => {
  return (
    <footer className="page-footer">
      <div className="footer-copyright">
        <div className="container">
          <span>ME - My Employee</span>
          <span className="right hide-on-small-only"> Created by Stella Vania at 3 March 2022</span>
        </div>
      </div>
    </footer>
  );
}

export default Footer;