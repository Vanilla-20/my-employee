const Header = () => {
  return (
    <header id="header" className="page-topbar">
      <div className="navbar-fixed">
          <nav className="navbar-color cyan">
              <div className="nav-wrapper">
                  <ul className="left">                      
                    <li>
                      <h1 className="logo-wrapper">
                        <a href="/#" className="brand-logo darken-1">My Employee</a>
                        <span className="logo-text">My Employee</span>
                      </h1>
                    </li>
                  </ul>
              </div>
          </nav>
      </div>
  </header>
  );
}

export default Header;