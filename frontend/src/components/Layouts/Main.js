import { Fragment, useEffect } from "react";
import Footer from "./Footer";
import Header from "./Header";
// import PageLoading from "./PageLoading";
import SideBar from "./Sidebar";
import Breadcrumbs from "../UI/Breadcrumbs";

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

const Main = (props) => {
  useEffect(() => {
    M.AutoInit();
  });

  return (
    <Fragment>
      {/* <PageLoading /> */}
      <Header />

      <div id="main">
        <div className="wrapper">
          <SideBar />

          <section id="content"> 
            <Breadcrumbs title={props.title} />
            <div className="container-fluid">
              <div className="row">
                <div className="col s12">{props.children}</div>
              </div>
            </div>
          </section>

        </div>
      </div>
      
      <Footer />
    </Fragment>
  );
}

export default Main;