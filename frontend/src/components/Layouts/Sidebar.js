import { Fragment, useState, useEffect } from "react";
// import { Link } from "react-router-dom";

import styles from './Sidebar.module.css';

import M from 'materialize-css';

const SideBar = () => {
  const sizeLevels = {
    mobile: 1,
    tablet: 2,
    desktop: 3,
    largeDesktop: 4,
  }
 
  //choose the screen size 
  const handleResize = () => {
    if (window.innerWidth <= 600) {
      setDeviceWidth(sizeLevels.mobile)
    } else if (window.innerWidth > 1200) {
      setDeviceWidth(sizeLevels.largeDesktop)
    } else if (window.innerWidth > 992) {
      setDeviceWidth(sizeLevels.desktop)
    } else if (window.innerWidth > 600) {
      setDeviceWidth(sizeLevels.tablet)
    }
  }
  const [deviceWidth, setDeviceWidth] = useState(4)

  useEffect(() => {
    var elems = document.getElementById('slide-out');
    M.Sidenav.init(elems, {});

    handleResize();
    window.addEventListener("resize", handleResize)
    return () => {
      window.removeEventListener('resize', handleResize);
    }
    // eslint-disable-next-line
  }, []);

  return (
    <Fragment>
      <aside id="left-sidebar-nav">
        <ul id="slide-out" className={`${deviceWidth > 2 ? 'side-nav '+styles.sidenav : 'sidenav'} fixed leftside-navigation`}>
          <li className="user-details cyan darken-2">
          <div className="row">
              <div className="col col s4 m4 l4">
                <img src={`${process.env.PUBLIC_URL}/styling/images/avatar.jpg`} alt="" className="circle responsive-img valign profile-image" />
              </div>
              <div className="col col s8 m8 l8">
                <ul id="profile-dropdown" className="dropdown-content">
                </ul>
                <a className="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="/#" data-activates="profile-dropdown">
                  John Doe
                  {/* <i className="mdi-navigation-arrow-drop-down right"></i> */}
                </a>
                <p className="user-roal">Administrator</p>
              </div>
          </div>
          </li>
          {/* <li className="bold"><Link to="/" className="waves-effect waves-cyan"><i className="mdi-action-dashboard"></i> Dashboard</Link></li>
          <li className="bold"><Link to="/pegawai" className="waves-effect waves-cyan"><i className="mdi-social-person"></i> Pegawai</Link></li>
          <li className="bold"><Link to="/cuti" className="waves-effect waves-cyan"><i className="mdi-editor-insert-invitation"></i> Cuti</Link></li>         */}
          <li className="bold"><a href="/" className="waves-effect waves-cyan"><i className="mdi-action-dashboard"></i> Dashboard</a></li>
          <li className="bold"><a href="/pegawai" className="waves-effect waves-cyan"><i className="mdi-social-person"></i> Pegawai</a></li>
          <li className="bold"><a href="/cuti" className="waves-effect waves-cyan"><i className="mdi-editor-insert-invitation"></i> Cuti</a></li>
        </ul>
        <span href="/#" data-target="slide-out" className="sidebar-collapse sidenav-trigger hide-on-large-only white-text"><i className="mdi-navigation-menu" style={{fontSize: '1.7rem'}}></i></span>
      </aside>
    </Fragment>
  );
}

export default SideBar;