const Breadcrumbs = (props) => {
  return (
    <div id="breadcrumbs-wrapper">
      {/* <!-- Search for small screen -->
      <div className="header-search-wrapper grey hide-on-large-only">
        <i className="mdi-action-search active"></i>
        <input type="text" name="Search" className="header-search-input z-depth-2" placeholder="Explore Materialize" />
      </div> */}
      <div className="container-fluid">
        <div className="row">
          <div className="col s12 m12 l12">
            <h1 className="breadcrumbs-title">{props.title}</h1>
            <ol className="breadcrumbs">{props.children}</ol>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Breadcrumbs;