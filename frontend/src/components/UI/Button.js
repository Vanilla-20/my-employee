const Button = props => {
  const classes = props.className + ' btn'
  return <button className={classes} onClick={props.onClick}>
      {props.children}
  </button>;
};

export default Button;