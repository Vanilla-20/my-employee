const DataTable = (props) => {
  return (
    <table id="data-table-simple" className="responsive-table display" cellSpacing="0">
      <thead>
          <tr>
            {/* {props.header.map((header) => {
              <th>{header}</th>
            })} */}
              <th>Name</th>
              <th>Position</th>
              <th>Office</th>
              <th>Age</th>
              <th>Start date</th>
              <th>Salary</th>
          </tr>
      </thead>
    
      <tbody>
        {/* {props.items.map((item) => {
          <tr>
            {Object.keys(item).forEach(key => {
              <td>{item[key]}</td>
            })}
          </tr>
        })} */}
        <tr>
          <td>Tiger Nixon</td>
          <td>System Architect</td>
          <td>Edinburgh</td>
          <td>61</td>
          <td>2011/04/25</td>
          <td>$320,800</td>
        </tr>
      </tbody>
    </table>
  );
}

export default DataTable;