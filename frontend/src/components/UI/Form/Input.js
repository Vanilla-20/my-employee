const Input = (props) => {
  return (
    <div className="row">
      <div className={props.className + ' input-field col s12'}>
        <input 
          id={props.id}
          type={props.type}
          placeholder={props.placeholder}
          name={props.name}
          value={props.value}
          onChange={props.onChange}
          onSelect={props.onSelect}
          disabled={props.disabled ?? false}
          readOnly={props.readOnly ?? false}
        />
        <label htmlFor={props.htmlFor}>{props.label}</label>
      </div>
    </div>
  );
}

export default Input;