import React, { Fragment, useState } from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';

Modal.setAppElement('#root-modals');

const CustomModal = (props) => {
  let subtitle;
  const [modalIsOpen, setIsOpen] = useState(false);

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = '#f00';
  }

  const openModal = () => {setIsOpen(true); }
  const closeModal = () => { setIsOpen(false); }

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      background: 'white',
      transform: 'translate(-50%, -50%)',
    },
  };

  return (
    <Fragment>
      <button className="btn" onClick={openModal}>Open Modal</button>
      {ReactDOM.createPortal(
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          contentLabel="Example Modal"
          customStyles={customStyles}
        >
          <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Hello</h2>
          <button onClick={closeModal}>close</button>
          <div>I am a modal</div>
        </Modal>,
      document.getElementById('root-modals'))}
    </Fragment>
  );
}

export default CustomModal;