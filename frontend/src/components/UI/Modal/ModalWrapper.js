import React, { Fragment, useState } from "react";
import ReactDOM from 'react-dom';

import Modal, { ModalBody, ModalFooter, ModalHeader } from "./Modal";
import Button from "../Button";

const ModalWrapper = (props) => {
  const [showModal, setShowModal] = useState(false);

  return (
    <Fragment>
      <Button onClick={() => setShowModal(true)}>{props.buttonText}</Button>

      {ReactDOM.createPortal(
        <Modal show={showModal} setShow={setShowModal}>
          <ModalHeader>{props.modalHeader}</ModalHeader>
          <ModalBody>{props.modalBody}</ModalBody>
          <ModalFooter>
            {props.modalFooter}
            {/* <Button onClick={() => setShowModal(false)}>Close</Button> */}
          </ModalFooter>
        </Modal>,
        document.getElementById('root-modals')
      )}
    </Fragment>
  );
}

export default ModalWrapper;