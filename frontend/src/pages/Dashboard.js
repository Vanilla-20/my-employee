import React, { useState, useEffect } from "react";
import Main from "../components/Layouts/Main";
import DataTable from 'react-data-table-component';
import axios from 'axios';

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

import Input from "../components/UI/Form/Input";

const Dashboard = (props) => {
  const [dataPegawaiTerawal, setDataPegawaiTerawal] = useState([]);
  const [dataCuti, setDataCuti] = useState([]);
  const [dataCuti2, setDataCuti2] = useState([]);
  const [dataSisaCuti, setDataSisaCuti] = useState([]);
  
  const [qty, setQty] = useState(3);
  const [year, setYear] = useState(2020);

  const columnPegawaiTerawal = [
    {
      name: 'Nomor Induk',
      selector: row => row.nomor_induk,
      sortable: true,
    },
    {
      name: 'Nama',
      selector: row => row.nama,
      sortable: true,
    },
    {
      name: 'Tanggal Bergabung',
      selector: row => row.tanggal_bergabung,
      sortable: true,
    },
  ];

  const columnCuti = [
    {
      name: 'Nomor Induk',
      selector: row => row.nomor_induk,
      sortable: true,
    },
    {
      name: 'Nama',
      selector: row => row.nama,
      sortable: true,
    },
    {
      name: 'Tanggal Cuti',
      selector: row => row.tanggal_cuti,
      sortable: true,
    },
    {
      name: 'Keterangan',
      selector: row => row.keterangan,
      sortable: true,
    },
  ];

  const columnSisaCuti = [
    {
      name: 'Nomor Induk',
      selector: row => row.nomor_induk,
      sortable: true,
    },
    {
      name: 'Nama',
      selector: row => row.nama,
      sortable: true,
    },
    {
      name: 'Sisa Cuti',
      selector: row => row.sisa_cuti,
      sortable: true,
    },
  ];

  useEffect(() => {
    const fetchFirstJoining = () => {
      axios.get(`${process.env.REACT_APP_API_URL}api/pegawai/stats/first_joining?qty=${qty}`).then((response) => { 
        setDataPegawaiTerawal(response.data.data);
      });
    }

    fetchFirstJoining();
  }, [qty]);

  useEffect(() => {
    const fetchHaveEverLeft = () => {
      axios.get(`${process.env.REACT_APP_API_URL}api/pegawai/stats/have_ever_left`).then((response) => {
        const data = response.data.data.map((cuti) => {
          return {
            nomor_induk   : cuti.nomor_induk,
            nama          : cuti.pegawai.nama,
            tanggal_cuti  : cuti.tanggal_cuti,
            keterangan    : cuti.keterangan,
          }
        })
        setDataCuti(data);
      });
    }

    const fetchHaveEverLeft2 = () => {
      axios.get(`${process.env.REACT_APP_API_URL}api/pegawai/stats/have_ever_left2`).then((response) => {
        const data = response.data.data.map((cuti) => {
          return {
            nomor_induk   : cuti.nomor_induk,
            nama          : cuti.pegawai.nama,
            tanggal_cuti  : cuti.tanggal_cuti,
            keterangan    : cuti.keterangan,
          }
        })
        setDataCuti2(data);
      });
    }

    fetchHaveEverLeft();
    fetchHaveEverLeft2();
  }, []);

  useEffect(() => {
    M.updateTextFields();
    const fetchRemainingLeaves = () => {
      axios.get(`${process.env.REACT_APP_API_URL}api/pegawai/stats/remaining_leaves?year=${year}`).then((response) => { 
        setDataSisaCuti(response.data.data);
      });
    }
    
    fetchRemainingLeaves();
  }, [year])

  return (
    <Main title={props.title}>
      <div className="section">
        <Input type="number" value={qty} onChange={(e) => { setQty(e.target.value); }} htmlFor="qty" label="Jumlah" />
        <h4 className="header">Pegawai Terawal Bergabung</h4>
        <DataTable columns={columnPegawaiTerawal} data={dataPegawaiTerawal} pagination />
        <h4 className="header">Pegawai Pernah Cuti</h4>
        <DataTable columns={columnCuti} data={dataCuti} pagination />
        <h4 className="header">Pegawai Pernah Cuti Lebih Dari Sekali</h4>
        <DataTable columns={columnCuti} data={dataCuti2} pagination />
        <h4 className="header">Sisa Cuti Pegawai</h4>
        <Input type="number" value={year} onChange={(e) => { setYear(e.target.value); }} htmlFor="year" label="Tahun" />
        <DataTable columns={columnSisaCuti} data={dataSisaCuti} pagination />
      </div>
    </Main>
  );
}

export default Dashboard;