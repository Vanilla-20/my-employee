import { useNavigate } from "react-router-dom";
import axios from 'axios';

import Main from "../../components/Layouts/Main";
import CutiForm from "./CutiForm";

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

const AddCuti = (props) => {
  const navigate = useNavigate();

  const submitHandler = (cuti) => {
    axios.post(`${process.env.REACT_APP_API_URL}api/cuti`, cuti)
      .then((response) => {
        M.toast({html: response.data.message});
        navigate('/cuti');
      })
      .catch((error) => {
        M.toast({html: error.response.data.message});
    });
  }

  return (
    <Main title={props.title}>
      <div className="section">
        <div className="row" style={{ padding: '15px' }}>
          <CutiForm onSubmit={submitHandler} />
        </div>
      </div>
    </Main>
  );
}

export default AddCuti;