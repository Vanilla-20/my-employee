import React, { Fragment, useState, useEffect } from "react";

import Main from "../../components/Layouts/Main";

import DataTable from 'react-data-table-component';
import axios from 'axios';
import { Link } from 'react-router-dom';

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";
import Button from "../../components/UI/Button";

const Cuti = (props) => {
  const [dataCuti, setDataCuti] = useState([]);

  const columns = [
    {
      name: 'Nomor Induk',
      selector: row => row.nomor_induk,
      sortable: true,
    },
    {
      name: 'Nama',
      selector: row => row.nama,
      sortable: true,
    },
    
    {
      name: 'Tanggal Cuti',
      selector: row => row.tanggal_cuti,
      sortable: true,
    },
    {
      name: 'Lama Cuti',
      selector: row => row.lama_cuti,
      sortable: true,
    },
    {
      name: 'Keterangan',
      selector: row => row.keterangan,
      sortable: true,
    },
    {
      name: 'Actions',
      selector: row => row.actions,
    },
  ]

  const fetchDataCuti = () => {
    axios.get(`${process.env.REACT_APP_API_URL}api/cuti`).then((response) => { 
      let data = response.data.data.map((cuti) => {
        return { 
          id          : cuti.id,
          nama        : cuti.pegawai.nama,
          nomor_induk : cuti.nomor_induk,
          tanggal_cuti: cuti.tanggal_cuti,
          lama_cuti   : cuti.lama_cuti,
          keterangan  : cuti.keterangan,
          actions: (
            <Fragment>
              <Link to={`${process.env.PUBLIC_URL}/cuti/${cuti.id}/edit`} ><button className="btn-floating green white-text"><i className="mdi-editor-mode-edit"></i></button></Link>
              <button className="btn-floating red white-text" onClick={() => {deleteCutiHandler(cuti.id)}}><i className="mdi-action-delete"></i></button>
            </Fragment>
          )
        }
      });
      setDataCuti(data);
    });
  }

  const deleteCutiHandler = (id) => {
    axios.delete(`${process.env.REACT_APP_API_URL}api/cuti/${id}`).then((response) => {
      M.toast({html: response.data.message});
      fetchDataCuti();
    });
  }

  useEffect(() => {
    fetchDataCuti();
    // eslint-disable-next-line
  }, []);
  
  return (
    <Main title={props.title}>
      <div className="section">
        <h4 className="header">Daftar Cuti</h4>
        <Link to="/cuti/add">
          <Button className="amber darken-2"><i className='material-icons left mdi-content-add'></i>Tambah</Button>
        </Link>
        <DataTable columns={columns} data={dataCuti} pagination />
      </div>      
    </Main>
  );
}

export default Cuti;