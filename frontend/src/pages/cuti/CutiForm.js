import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
// import Autocomplete from 'react-autocomplete';

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";
import $ from 'jquery';

import Input from "../../components/UI/Form/Input";

const CutiForm = (props) => {
  const [cuti, setCuti] = useState({
    id                : '',
    nomor_induk       : '',
    tanggal_cuti      : '',
    lama_cuti         : '',
    keterangan        : '',
  });
  // const [dataPegawai, setDataPegawai] = useState([]);

  useEffect(() => {
    const fetchCuti = () => {
      axios.get(`${process.env.REACT_APP_API_URL}api/cuti/${props.cutiId}`).then((response) => { 
        let data = response.data.data;
        setCuti(data);
        M.updateTextFields();
      });
    }

    if (props.cutiId !== undefined) fetchCuti();
    
    $(document).ready(() => {
      var elems = document.querySelectorAll('.datepicker');
      M.Datepicker.init(elems, {
        autoClose: true,
        format: "yyyy-mm-dd",
        yearRange: [1930, new Date().getFullYear()],
        defaultDate: new Date(),
        onClose: (date) => {
          //
        },
        onSet: (date) => {
          //
        },
      });
    });
  }, [props.cutiId]);

  // useEffect(() => {
  //   const fetchDataPegawai = () => {
  //     axios.get(`${process.env.REACT_APP_API_URL}api/pegawai`).then((response) => { 
  //       let data = response.data.data;
  //       setDataPegawai(data);
  //     });
  //   }

  //   fetchDataPegawai();
  // }, []);

  const changeHandler = (e) => {
    const id = e.target.id;
    setCuti((prev) => {
      prev[id] = e.target.value;
      return {
        ...prev
      }
    })
  }

  const submitHandler = (e) => {
    e.preventDefault();
    props.onSubmit(cuti);
    setCuti({
      id                : '',
      nomor_induk       : '',
      tanggal_cuti      : '',
      lama_cuti         : '',
      keterangan        : '',
    });
  }

  return (
    <form className="card col s12" onSubmit={submitHandler}>
      <div className="section">
        <Link to="/cuti"><i className="mdi-navigation-arrow-back" style={{fontSize: '1.75rem', color: "gray"}}></i></Link>
      </div>
      <div className="divider"></div>
      <div style={{padding: '1rem 1.75rem'}}>
        <Input
          id="nomor_induk"
          type="text" 
          htmlFor="nomor_induk"
          label="Nomor Induk"
          onChange={changeHandler}
          value={cuti.nomor_induk}
        />
        {/* <Autocomplete
          value={cuti.nomor_induk}
          items={dataPegawai}
          getItemValue={item => item.nomor_induk}
          shouldItemRender={(item, isHighlighted) => {
            <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
              {item.nama}
            </div>
          }}
          renderMenu={item => (
            <div className="dropdown">
              {item}
            </div>
          )}
          renderItem={(item, isHighlighted) =>
            <div className={`item ${isHighlighted ? 'selected-item' : ''}`}>
              {item.title}
            </div>
          }
          onChange={(event, val) => {
            setCuti((prev) => {
              prev.nomor_induk = event.target.value;
              return {
                ...prev
              }
            })
          }}
        /> */}
        <Input 
          id="tanggal_cuti"
          type="date"
          htmlFor="tanggal_cuti"
          label="Tanggal Cuti"
          // className="datepicker"
          // placeholder="Format: yyyy-mm-dd"
          onChange={changeHandler}
          value={cuti.tanggal_cuti}
        />
        <Input 
          id="lama_cuti"
          type="number"
          htmlFor="lama_cuti"
          label="Lama Cuti"
          // className="datepicker"
          // placeholder="Format: yyyy-mm-dd"
          onChange={changeHandler}
          value={cuti.lama_cuti}
        />
        <Input 
          id="keterangan"
          type="text"
          htmlFor="keterangan"
          label="Keterangan"
          // className="datepicker"
          // placeholder="Format: yyyy-mm-dd"
          onChange={changeHandler}
          value={cuti.keterangan}
        />
        <div className="row">
          <div className="input-field col s12">
            <button className="btn amber darken-2 waves-effect waves-light right" type="submit" name="action">Submit
              <i className="mdi-content-send right"></i>
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

export default CutiForm;