import { useNavigate } from "react-router-dom";
import axios from 'axios';

import Main from "../../components/Layouts/Main";
import PegawaiForm from "./component/PegawaiForm";

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

const AddPegawai = (props) => {
  const navigate = useNavigate();

  const submitHandler = (pegawai) => {
    axios.post(`${process.env.REACT_APP_API_URL}api/pegawai`, pegawai)
      .then((response) => {
        M.toast({html: response.data.message});
        navigate('/pegawai');
      })
      .catch((error) => {
        M.toast({html: error.response.data.message});
    });
  }

  return (
    <Main title={props.title}>
      <div className="section">
        <div className="row" style={{ padding: '15px' }}>
          <PegawaiForm onSubmit={submitHandler} />
        </div>
      </div>
    </Main>
  );
}

export default AddPegawai;