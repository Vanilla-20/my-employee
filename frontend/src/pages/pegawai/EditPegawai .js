import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from 'axios';

import Main from "../../components/Layouts/Main";
import PegawaiForm from "./component/PegawaiForm";

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

const EditPegawai = (props) => {
  const { id } = useParams();
  const navigate = useNavigate();

  const submitHandler = (pegawai) => {
    axios.put(`${process.env.REACT_APP_API_URL}api/pegawai/${id}`, pegawai)
      .then((response) => {
        M.toast({html: response.data.message});
        navigate('/pegawai');
      })
      .catch((error) => {
        M.toast({html: error.response.data.message});
    });
  }

  useEffect(() => {
    var elems = document.querySelectorAll('.datepicker');
    M.Datepicker.init(elems, {
      selectYears: 50,
      defaultDate: new Date(),
    });
  }, []);

  return (
    <Main title={props.title}>
      <div className="section">
        <div className="row" style={{ padding: '15px' }}>
          <PegawaiForm onSubmit={submitHandler} pegawaiId={id} />
        </div>
      </div>
    </Main>
  );
}

export default EditPegawai;