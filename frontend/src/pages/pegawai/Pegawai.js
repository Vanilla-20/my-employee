import React, { Fragment, useState, useEffect } from 'react';

import Main from "../../components/Layouts/Main";
// import ModalWrapper from '../../components/UI/Modal/ModalWrapper';
// import Input from '../../components/UI/Form/Input';

import DataTable from 'react-data-table-component';
import axios from 'axios';
import Button from '../../components/UI/Button';
import { Link } from 'react-router-dom';

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

const Pegawai = (props) => {
  const columns = [
    {
      name: 'Nomor Induk',
      selector: row => row.nomor_induk,
      sortable: true,
    },
    {
      name: 'Nama',
      selector: row => row.nama,
      sortable: true,
    },
    {
      name: 'Alamat',
      selector: row => row.alamat,
      sortable: true,
    },
    {
      name: 'Tanggal Lahir',
      selector: row => row.tanggal_lahir,
      sortable: true,
    },
    {
      name: 'Tanggal Bergabung',
      selector: row => row.tanggal_bergabung,
      sortable: true,
    },
    {
      name: 'Actions',
      selector: row => row.actions,
    },
  ];

  const [dataPegawai, setDataPegawai] = useState([]);

  const fetchDataPegawai = () => {
    axios.get(`${process.env.REACT_APP_API_URL}api/pegawai`).then((response) => { 
      let data = response.data.data.map((pegawai) => {
        return { 
          ...pegawai,
          actions: (
            <Fragment>
              <Link to={`${process.env.PUBLIC_URL}/pegawai/${pegawai.nomor_induk}/edit`} ><button className="btn-floating green white-text"><i className="mdi-editor-mode-edit"></i></button></Link>
              <button className="btn-floating red white-text" onClick={() => {deletePegawaiHandler(pegawai.nomor_induk)}}><i className="mdi-action-delete"></i></button>
            </Fragment>
          )
        }
      });
      setDataPegawai(data);
    });
  }

  const deletePegawaiHandler = (nomor_induk) => {
    axios.delete(`${process.env.REACT_APP_API_URL}api/pegawai/${nomor_induk}`).then((response) => {
      M.toast({html: response.data.message});
      fetchDataPegawai();
    });
  }

  useEffect(() => {
    fetchDataPegawai();
    // eslint-disable-next-line
  }, []);

  return (
    <Main title={props.title}>
      <div className="section">
        <h4 className="header">Daftar Pegawai</h4>
        <Link to="/pegawai/add">
          <Button className="amber darken-2"><i className='material-icons left mdi-content-add'></i>Tambah</Button>
        </Link>
        {/* <ModalWrapper
          buttonText={<><i className='material-icons left mdi-content-add'></i>Tambah</>}
          modalHeader={<><h5>Tambah Pegawai Baru</h5></>}
          modalBody={<>
            <div className="row">
              <div className="col s12">
                <h4 className="header2">Isi Data di Bawah Ini</h4>
                <div className="row">
                  <form className="col s12" onSubmit={addPegawaiHandler}>
                    <Input id="nama" type="text" htmlFor="nama" label="Nama" />
                    <Input id="alamat" type="text" htmlFor="alamat" label="Alamat" />
                    <Input id="tanggal_lahir" type="text" htmlFor="tanggal_lahir" label="Tanggal Lahir" placeholder="Tanggal Lahir" className="datepicker" />
                    <div className="row">
                      <div className="input-field col s12">
                        <button className="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                          <i className="mdi-content-send right"></i>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </>} 
        /> */}
        <DataTable columns={columns} data={dataPegawai} pagination />
      </div>      
    </Main>
  );
}

export default Pegawai;