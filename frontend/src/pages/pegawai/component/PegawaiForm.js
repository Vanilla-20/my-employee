import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import M from 'materialize-css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";
import $ from 'jquery';

import Input from "../../../components/UI/Form/Input";

const PegawaiForm = (props) => {
  const [pegawai, setPegawai] = useState({
    nomor_induk       : '',
    nama              : '',
    alamat            : '',
    tanggal_lahir     : '',
    tanggal_bergabung : '',
  });

  useEffect(() => {
    const fetchPegawai = () => {
      axios.get(`${process.env.REACT_APP_API_URL}api/pegawai/${props.pegawaiId}`).then((response) => { 
        let data = response.data.data;
        setPegawai(data);
        M.updateTextFields();
      });
    }

    if (props.pegawaiId !== undefined) fetchPegawai();
    
    $(document).ready(() => {
      var elems = document.querySelectorAll('.datepicker');
      M.Datepicker.init(elems, {
        autoClose: true,
        format: "yyyy-mm-dd",
        yearRange: [1930, new Date().getFullYear()],
        defaultDate: new Date(),
        onClose: (date) => {
          //
        },
        onSet: (date) => {
          //
        },
      });
    });
  }, [props.pegawaiId]);

  const changeHandler = (e) => {
    const id = e.target.id;
    setPegawai((prev) => {
      prev[id] = e.target.value;
      return {
        ...prev
      }
    })
  }

  const submitHandler = (e) => {
    e.preventDefault();
    props.onSubmit(pegawai);
    setPegawai({
      nama              : '',
      alamat            : '',
      tanggal_lahir     : '',
      tanggal_bergabung : '',
    });
  }

  return (
    <form className="card col s12" onSubmit={submitHandler}>
      <div className="section">
        <Link to="/pegawai"><i className="mdi-navigation-arrow-back" style={{fontSize: '1.75rem', color: "gray"}}></i></Link>
      </div>
      <div className="divider"></div>
      <div style={{padding: '1rem 1.75rem'}}>
        {props.pegawaiId !== undefined && 
          <Input
            id="nomor_induk"
            type="text" 
            htmlFor="nomor_induk"
            label="Nomor Induk"
            // onChange={changeHandler}
            value={pegawai.nomor_induk}
            placeholder="Nomor Induk"
            readOnly={true}
          />
        }
        <Input 
          id="nama" 
          type="text" 
          htmlFor="nama" 
          label="Nama" 
          onChange={changeHandler} 
          value={pegawai.nama}
        />
        <Input 
          id="alamat" 
          type="text" 
          htmlFor="alamat" 
          label="Alamat" 
          onChange={changeHandler}
          value={pegawai.alamat}
        />
        <Input 
          id="tanggal_lahir"
          type="date"
          htmlFor="tanggal_lahir"
          label="Tanggal Lahir"
          // className="datepicker"
          // placeholder="Format: yyyy-mm-dd"
          onChange={changeHandler}
          value={pegawai.tanggal_lahir}
        />
        <Input 
          id="tanggal_bergabung"
          type="date"
          htmlFor="tanggal_bergabung"
          label="Tanggal Bergabung"
          // className="datepicker"
          // placeholder="Format: yyyy-mm-dd"
          onChange={changeHandler}
          value={pegawai.tanggal_bergabung} 
        />
        <div className="row">
          <div className="input-field col s12">
            <button className="btn amber darken-2 waves-effect waves-light right" type="submit" name="action">Submit
              <i className="mdi-content-send right"></i>
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

export default PegawaiForm;